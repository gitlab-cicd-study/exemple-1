# Exemple-1

## Pipeline e Jobs
O arquivo .gitlab-ci.yml é responsável por criar toda a PIPELINE que vai conter os JOBS com cada um de sesu comandos em serie, seja através de script ou outros comandos. <br>
exemplo de job - execute_test, create_images e push_images <br>
<br>

## Stages
Quando não especificado os STAGES, os JOBS vão rodar paralelamentes, não tendo necessidade de um JOB rodar apenas se outro for concluido. Para isso utilizamos STAGES que especifica a ordem em que os JOBS devem ser executados, impedindo que um JOB seja executado caso o JOB que é necessário falhe. Para isso crie uma tag stages fora da lista de JOBS e liste a sequencia de STAGES que deve ser atendido. Dentro de cada JOB especifique através da tag stage qual stage pertence ao JOB .
![Alt text](image.png)

## Dependencias 
As dependencias são utilizadas para que um JOB seja executado apenas se um outro Job for concluido sem erros. Funciona como se fosse STAGES porém sem a necessidade de criar novos STAGES, sendo assim utilizado apenas os nomes dos JOBS existentes. Para isso basta criar a tag neeeds dentro de um JOB que precisa esperar a conclusão de outro JOB e referencias o needs com o nome do JOB que deve ser concluido previamente.
![Alt text](image-1.png)
![Alt text](image-3.png)

## Scripts
Os scripts estão dentro dos JOBS e eles são executados em sistema operacional Linux. Para isso uma container docker é criado e executado toda vez que a pipeline roda, não sendo salvo informações alguma (É utilizada uma imagem do Linux). <br>
Os scripts também podem ser executados a partir de um script preparado em um arquivo diferente do .gitlab-ci.yml e para isso vc importa o caminho desse arquivo na tag script do JOB desejado. ex. - ./script.sh<br>
Por ser sistema Linux deve ser adicionado permissão de execução através da do comando - chmod +x ./script.sh dentro do before_script.
 
## Only
A tag ONLY é utilizada dentro dos JOBS para identificar em quais branchs esses JOBS serão executados <br>
Para utilizar abrimos a tag only: dentro do job desejado e referenciamos a branch pulando uma linha. Caso o JOB não tenha a tag only então este job será em todas as branchs criadas.

## Workflow
A tag workflow é utilizada na raiz do .gitlab-ci.yml para determianr regras de quando a pipeline com os jobs serão utilizados, simplificando o uso excessivo de ONLY. Para isso utilizamos a tag workflow e em seguida pulando linha a tag rules que deve receber condicionais para executar a pipeline.
![Alt text](image-2.png)
Nesse caso é feito uma condicional com if para verificar se a BRANCH é main, caso seja será executado todos os JOBS, caso contrario não sera executado nenhum JOB.

## Images
Por padrão o gitlab-ci.yml puxa uma imagem do ruby para montar o container e se especificar um comando npm para checagem de versão teremos erro, ja que o ruby não possui este comando. Para isso podemos especificar no inicio do arquivo a tag IMAGE e passar a versão da imagem node que queremos (Será aplicado em todos os JOBS), sendo pego essas informações do Docker Hub (Igual a uma configuração de imagem docker).<br>
É possível utilizar as IMAGENS em determinados JOBS apenas, basta declarar a tag IMAGE dentro do job e não na raiz do arquivo.
![Alt text](image-2.png)
![Alt text](image-4.png)